 /** 
  * @title Users API
  * @description Endpoints for getting a user object, user registration, and user authentication.
  * @author ethancrist
  **/

'use strict'

// [DEPENDENCIES]
const micro = require('iomicro')
const Redis = require('ioredis')
const users = new Redis({ host: '50.30.37.59', password: process.env.REDIS_AUTH, db: 0 })

// [FUNCTIONS]
/**
 * @purpose Removes sensitive data fields, i.e. password, of a full user object for public use.
 **/
const filterUserObject = function(fullUserObject) {
    const fieldsToRemove = [ 'password' ]

    for (let i = 0, len = fieldsToRemove.length; i < len; i++) {
        delete fullUserObject[fieldsToRemove[i]]
    }

    return fullUserObject
}

/**
 * @purpose Update certain fields of an object while falling back to defaults for unpatched fields. 
 * @usage patchObject({ field1: 'one', field2: 'updateMe', field3: 'three' }, { field2: 'two' })
 * @return { field1: 'one', field2: 'two', field3: 'three' }
 **/
const patchObject = function(fallbackObject, patchedFields) {
    // This allows the request to only update one field at a time
    const newObject = fallbackObject
    const keys = Object.keys(patchedFields)

    for (let i = 0, len = keys.length; i < len; i++) {
        // "Patching" the fields in the new user object with the fields sent in
        newObject[keys[i]] = patchedFields[keys[i]] 
    }

    return newObject
}

/**
 * @purpose Send a "user not found message".
 **/
const userNotFound = function(res) {
    res.status(404).send('No index for that username.') 
}

/**
 * @purpose Determine if an object is empty or not.
 **/
const objectIsEmpty = function(object) {
    return !object || Object.keys(object) < 1
}

/**
 * @purpose Hash a raw password.
 * @return Hashed password.
 **/
const hashPassword = function(password) {
    return micro.encrypt(password, process.env.PASSWORDS_HASH)
}


// [ENDPOINTS]

// Get a user object
micro.get('/users/:username', { private: true }, (req, res) => {
    // Attempting to pull the user from the database with the given tusername.
    users.hgetall(req.params.username).then((user) => {
        if (objectIsEmpty(user)) userNotFound(res)

        // User exists; sending a 200.
        // Removing sensitive fields before exposing object to endpoint 
        res.status(200).send(filterUserObject(user)) 
    }).catch(() => {
        res.status(500).send('Could not fetch user, please check your request and try again.') 
    })
})

// User registration
micro.post('/users/', { private: true }, (req, res) => {
    // Just remove or add a required field in this array, and it will be looped through/checked automatically
    const requiredFields = [ 'email', 'username', 'password']

    // Gatekeeping to ensure all required fields are set
    for (let i = 0, len = requiredFields.length; i < len; i++) {
        if (!req.body[requiredFields[i]]) return res.status(400).send('Missing required field: '+requiredFields[i])
    }

    // Hashing the password if sent
    if (req.body.password) req.body.password = hashPassword(req.body.password)
    
    // Checking to see if the user exists before posting...
    users.hgetall(req.body.username).then((user) => {
        if (!objectIsEmpty(user)) return res.status(302).send('Index already exists; use the PATCH method to update it.')

        // Username doesn't already exist; saving the user in the database 
        users.hmset(req.body.username, req.body)

        // Sending a 201 response with the non-sensitive user object
        res.status(201).send(filterUserObject(req.body))

    }).catch(() => {
        res.status(500).send('Could not create user, please check your request and try again.') 
    })
})

// User modification
micro.patch('/users/:username', { private: true }, (req, res) => {
    // Will update the user object if:
    //      1) The username given in the endpoint URL exists
    //          If it doesn't, will send a 404 and NOT update
    //      2) The new desired username in req.body does NOT exist
    //          If it does, will send a 302 and NOT update
    
    // Hashing the password if sent
    if (req.body.password) req.body.password = hashPassword(req.body.password)

    users.hgetall(req.params.username).then((oldUser) => { 
        // This user object didn't exist; can't patch
        if (objectIsEmpty(oldUser)) return res.status(404).send('Index doesn\'t exist; use the POST method to create it.')

        const newUser = patchObject(oldUser, req.body)

        const preventDuplicateUsernames = new Promise(function(resolve, reject) {
            // Since the username must be unique, checking to see if there is already an index for the new desired username.
            users.hgetall(req.body.username).then((newUserCheck) => { 

                // Making sure the new desired username doesn't already exist.
                if (!objectIsEmpty(newUserCheck)) return reject()

                // Deleting the old username index.
                users.del(req.params.username)

                // Adding the previous username to the usernameHistory of the new username index.
                const newUsernameHistory = newUser.usernameHistory || []
                newUsernameHistory.push(req.params.username)
                newUser.usernameHistory = newUsernameHistory

                // Upating database and sending successful response 
                resolve(newUser)                
            })
        })
        
        const saveAndSend = function() {
            // Updating the user in the database and sending a 201 response.
            users.hmset(newUser.username, newUser)

            // Sending a 201 with the non-sensitive data of the new user object
            res.status(201).send(filterUserObject(newUser))
        }

        // If username in body is not null and mismatches the one in the URL, then the username IS being updated.
        if (req.body.username && req.params.username !== req.body.username) {
            return preventDuplicateUsernames.then(saveAndSend).catch(function() {
                res.status(302).send('Index "'+req.body.username+'" already exists; choose another.')
            })
        }

        saveAndSend()
    }).catch(() => { 
        res.status(500).send('Could not modify user, please check your request and try again.') 
    })
})

// User authorization
micro.post('/users/auth', { private: true }, (req, res) => {
    // Hashing the password if sent
    if (req.body.password) req.body.password = hashPassword(req.body.password)
    
    users.hgetall(req.body.username).then((user) => {
        if (!user.password) userNotFound(res)

        // Note: req.body.password is hashed above to match with the DB user.password which is hashed in the DB
        const authorized = req.body.password === user.password

        res.status(authorized ? 200 : 401).send({ authorized: authorized })
    }).catch(() => {
        res.status(500).send('Could not authorize user, please check your request and try again.')
    })
})

micro.listen(3000, {
    appName: 'Users API',
    hello: process.env.HELLO || 'All endpoints are online.',
    ssl: {
        key: '/etc/letsencrypt/live/narrownode.org/privkey.pem',
        cert: '/etc/letsencrypt/live/narrownode.org/fullchain.pem'
    },
    apiKeysFile: '../keys/users.pem'
})
